// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2021, CZ.NIC z.s.p.o. (http://www.nic.cz/)
#ifndef _LIBFOO_LOG_H_
#define _LIBFOO_LOG_H_
#define DEFLOG log_libfoo
#include <logc.h>
#include <logc_util.h>

extern log_t log_libfoo;

#endif
