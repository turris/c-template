= C project template
CZ.NIC z.s.p.o. <packaging@turris.cz>
v0.0, 2021-07-09
:icons:

This is description of your project. Do NOT forget to modify it together with
title of this file, project dependencies and other parts of this file which
would require the change.  


== Dependencies

* https://mesonbuild.com/[Meson build system]
* https://www.gnu.org/software/gperf[gperf]
* https://gitlab.nic.cz/turris/logc[LogC]
* http://www.hyperrealm.com/libconfig/libconfig.html[libconfig]
* On non-glibc http://www.lysator.liu.se/~nisse/misc[argp-standalone]

For tests:

* https://libcheck.github.io/check[check]
* https://bats-core.readthedocs.io/en/stable/index.html[bats]
* Optionally http://www.valgrind.org[valgrind]

For code coverage report:

* http://ltp.sourceforge.net/coverage/lcov.php[lcov]

For linting:

* https://github.com/danmar/cppcheck[cppcheck]
* https://dwheeler.com/flawfinder/[flawfinder]


== Compilation

To compile this project you have to run:

----
meson setup builddir
meson compile -C builddir
----

Subsequent installation can be done with `meson install -C builddir`.


== Running tests

This project contains basic tests in directory tests.

To run tests you have to either use `debug` build type (which is commonly the
default for meson) or explicitly enable them using `meson configure
-Dtests=enabled builddir`. To execute all tests run:

----
meson test -C builddir
----

You can also run tests with Valgrind tool such as `memcheck`:

----
VALGRIND=memcheck meson test -C builddir
----

=== Code coverage report

There is also possibility to generate code coverage report from test cases. To
do so you can run:

----
meson setup -Db_coverage=true builddir
meson test -C builddir
ninja -C builddir coverage-html
----

The coverage report is generated in directory:
`builddir/meson-logs/coveragereport`.

== Linting the code

The code can also be linted if linters are installed. There are two linter
supported at the moment. There is `cppcheck` and `flawfinder`. To run them you
can do:

----
meson setup builddir
meson compile -C builddir cppcheck
meson compile -C builddir flawfinder
----
